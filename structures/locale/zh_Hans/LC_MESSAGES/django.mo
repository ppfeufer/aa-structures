��    /      �  C           =     6   W     �     �     �     �     �     �     �     �     �     
          #     /     <     C     I     P  	   Y     c     �     �     �     �  ;   �     �     �     �     �     �  	   	               !     -     B  d   G     �     �     �     �     �     �     �     �  �  �  8   �  4   �     �     	     	     +	     7	     >	     K	     X	     _	     o	     v	     �	     �	     �	     �	     �	     �	  	   �	     �	     �	     �	  	   �	     �	  0   �	  	   
     (
     5
     <
     C
     J
     V
     c
     j
     w
     �
  P   �
     �
     �
     �
          
          *     1              "   
                   +                             $       !          /                     '                                  &             (                 %             -   #   	   *                    )   .   ,          %(corporation)s was added as new structure owner by %(user)s. %(title)s: %(topic)s updated for %(owner)s: %(result)s All dates are EVE time Alliance Apply Filter Armor timer Cancel Chinese Simplified Clear Filter Corporation Discord Webhook English Extraction ready Final timer Fuel Expires German Group Korean Location Low Power Moon mining extraction started N/A OK Owner Region Reinforced structures (except POCOs) are highlighted in red Reinforced? Reset Filter Russian Services Solar System Sov timer Spanish State Tags Filter Tags Filter (Active) Type You can only use your main or alt characters to add corporations. However, character %s is neither.  completed successfully no offline online service is down service is up unknown yes Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-03-09 19:25+0000
Last-Translator: Erik Kalkoken <erik.kalkoken@gmail.com>, 2020
Language-Team: Chinese (China) (https://app.transifex.com/kalkoken-apps/teams/107978/zh_CN/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: zh_CN
Plural-Forms: nplurals=1; plural=0;
 %(corporation)s已被%(user)s添加为新建筑持有者 %(title)s: %(topic)s 更新为 %(owner)s: %(result)s 所有时间均为EVE时间 联盟 应用过滤 装甲timer 取消 简体中文 清除过滤 公司 Discord Webhook 英语 准备采集 最终timer 燃料消耗 德语 组  韩语 位置 低能量 月矿采集开始 N/A 好 所有人 星域 增强建筑（除了海关）将用红色高亮 增强？ 重置过滤 俄语 服务 星系 主权timer 西班牙文 状态 标签过滤 标签过滤（激活） 类别 你只能用你的大号或小号注册公司，但是%s不属于以上两种。 成功完成 没有 离线 线上 服务已离线 服务已上线 未知 是 